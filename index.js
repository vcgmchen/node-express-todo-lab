import express from 'express'
const app = express() // initializes the server object 
const port = 3000 

// allow us to parse json
app.use(express.json()) // tells express server to use json middleware

app.get('/', (req, res) => res.send('Hello world')) // tells server how to handle a get request at the root path. respond with this string
// if wanted to be able to handle e.g. POST request, would have: app.post()... 

app.listen(port, () => console.log(`API server ready on http://localhost:${port}`)) // callback that gets run when you connect to the port - runs once
